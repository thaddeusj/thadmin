import math
from decimal import Decimal
from datetime import datetime, timedelta
from pytz import utc
from uuid import uuid4
import re
import random
import ldap

import stripe

from thadmin.common.util import ldap_connect, ldap_close, dn_for_mail
from thadmin.accounts import models, tasks
from thadmin.common.processes import submit_process
from thadmin import settings

class PasswordSetFailed(Exception):
    pass

class CreateOrderError(Exception):
    pass

class PaymentDataInvalid(Exception):
    def __init__(self, field):
        Exception.__init__(self, "%s missing or invalid" % field)
        self.field = field

def assign_manager(email, account_id):
    mgr = models.AccountManager()
    mgr.email = email
    mgr.account_id = account_id
    mgr.save()

def remove_manager(email):
    mgr = models.AccountManager.objects.get(email = email).delete()

def account_id_for_manager(email):
    mgr = models.AccountManager.objects.get(email = email)
    return mgr.account_id

def manager_for_account_id(account_id):
    mgrs = models.AccountManager.objects.filter(account_id = account_id)
    emails = []
    for m in mgrs:
        emails.append(m.email)

    return emails

def create_invitation(issued_to_mail, issued_to_name):
    invite = models.Invitation()
    invite.invite_code = str(uuid4()).lower()
    invite.issued_to_mail = issued_to_mail
    invite.issued_to_name = issued_to_name
    invite.issued_datetime = datetime.now(tz = utc)
    invite.save()
    return invite.invite_code

def redeem_invitation(invite_code):
    try:
        invite = models.Invitation.objects.get(invite_code = invite_code.lower())
        if invite.redeemed_datetime is not None:
            return False
    except:
        return False

    invite.redeemed_datetime = datetime.now(tz = utc)
    invite.save()

    return True

def validate_password(password):
    # Rules
    # min_len - Length >= 8 chars
    # repeat - No single character repeated 3 or more times
    #
    # IF Length < 18 chars
    #   4letters - Contains at least four letters
    #   1number - Contains at least one numeral
    #   1symbol Contains at least one symbol
    #
    # longpass - Length >= 18 chars
    #
    # Response format:
    # {
    #   "valid" : true / false,
    #   "rules": {
    #       "rule_name": "pass/fail/na", ...
    #   }
    # }
    result = { 'valid': True, 'rules': {} }

    def r_fail(rule):
        result['valid'] = False
        result['rules'][rule] = 'fail'

    def r_pass(rule):
        result['rules'][rule] = 'pass'

    def r_na(rule):
        result['rules'][rule] = 'na'

    if len(password) < 8: r_fail('min_len')
    else: r_pass('min_len')

    if re.search(r'(.)\1{2,}', password) is not None:
        r_fail('repeat')
    else: r_pass('repeat')

    if len(password) < 18:
        r_na('longpass')
        
        if len(re.findall(r'[a-zA-Z]', password)) < 4:
            r_fail('4letters')
        else: r_pass('4letters')

        if len(re.findall(r'[0-9]', password)) < 1:
            r_fail('1number')
        else: r_pass('1number')

        if len(re.findall(r'[\!\@\#\$\%\^\&\*\(\)\-\=\_\+\[\]\{\}\\\|\;\'\:\"\,\.\/\<\>\?]', password)) < 1:
            r_fail('1symbol')
        else: r_pass('1symbol')
    else:
        r_na('4letters')
        r_na('1number')
        r_na('1symbol')
        r_pass('longpass')

    return result

def gen_account_id(last_name, first_name):
    dedupe_attempt = 0
    unique = False
    account_id = 'hs-%s-%s' % (last_name, first_name)
    account_id = account_id.lower()

    while dedupe_attempt < 5 and not unique:
        dedupe_attempt += 1

        if len(models.Account.objects.filter(account_id = account_id)) == 0:
            unique = True
        else:
            account_id_ext = random.randint(1000,9999)
            account_id = 'hs-%s-%s-%i' % (last_name, first_name, account_id_ext)
            account_id = account_id.lower()

    if not unique:
        raise Exception("Failed to generate a unique account ID")

    return account_id

def clean_account_data(account_data):
    errors = {}

    def must(field, name, max_len):
        if not field in account_data or len(account_data[field]) == 0:
            errors[field] = "%s must be specified" % name
        elif len(account_data[field]) > max_len:
            errors[field] = "%s maximum length %i characters, you entered %i" % (
                name, max_len, len(account_data[field]))

    must('first_name', 'First Name', 123)
    must('last_name', 'Last Name', 123)
    must('street_address', 'Street Address', 255)
    must('city', 'City', 100)
    must('state', 'State', 25)
    must('zipcode', 'ZIP Code', 11)
    must('phone', 'Phone Number', 25)

    if 'phone' in account_data:
        phone = re.sub(r'[^0-9]', '', account_data['phone'])
        if len(phone) < 10 or (len(phone) == 11 and not phone.startswith('1')) or len(phone) > 11:
            errors['phone'] = "Please enter a 10 digit phone number including area code"
        account_data['phone'] = phone

    return errors

def create_account(account_data):
    # Step 1 - Generate account ID and create database record
    account_id = gen_account_id(account_data['last_name'], account_data['first_name'])

    account = models.Account()
    account.account_id = account_id
    account.first_name = account_data['first_name']
    account.last_name = account_data['last_name']
    account.street_address = account_data['street_address']
    account.city = account_data['city']
    account.state = account_data['state']
    account.zipcode = account_data['zipcode']
    account.phone = account_data['phone']
    account.active = True
    account.save()

    # Step 2 - Create AccountManager record
    am = models.AccountManager()
    am.email = '%s@thogan.com' % account_id
    am.account_id = account_id
    am.save()

    # Step 3 - Connect to LDAP for directory operations
    ldap_conn = ldap_connect()

    # Step 4 - Create new account OUs
    users_ou_dn = 'ou=' + account_id + ',' + settings.LDAP_OU_USERS
    ldap_conn.add_s(users_ou_dn, [
        ('objectClass', ['organizationalUnit', 'top']),
        ('ou', str(account_id)) ])

    mailgroups_ou_dn = 'ou=' + account_id + ',' + settings.LDAP_OU_MAILGROUPS
    ldap_conn.add_s(mailgroups_ou_dn, [
        ('objectClass', ['organizationalUnit', 'top']),
        ('ou', str(account_id)) ])

    # Step 5 - Create account user
    account_user_dn = 'cn=' + account_id + ',' + users_ou_dn
    account_user_mail = account_id + '@thogan.com'

    ldap_conn.add_s(account_user_dn, [
        ('objectClass', ['hostedServicesAccount', 'inetOrgPerson',
            'organizationalPerson', 'person', 'top']),
        ('cn', str(account_id)),
        ('mail', str(account_user_mail)),
        ('displayName', str("%s %s" % (account.first_name, account.last_name))),
        ('sn', str(account.last_name)),
        ('givenName', str(account.first_name)),
        ('street', str(account.street_address)),
        ('l', str(account.city)),
        ('st', str(account.state)),
        ('postalCode', str(account.zipcode)),
        ('mobile', str(account.phone)) ])

    # Step 6 - Add new user to hosted services group
    ldap_conn.modify_s(settings.LDAP_HS_GROUP, [
        (ldap.MOD_ADD, 'uniqueMember', str(account_user_dn)) ])

    # Step 7 - Clean up
    ldap_close(ldap_conn)

    return account_id

def set_pwrs_token(session, valid_sec):
    td = timedelta(seconds = valid_sec)
    session['pwrs_token'] = datetime.now(tz = utc) + td

def validate_pwrs_token(session):
    exp = session['pwrs_token']
    del session['pwrs_token']
    return datetime.now(tz = utc) < exp

def set_password(mail, password):
    rules = validate_password(password)
    if not rules['valid']:
        raise PasswordSetFailed("Password does not meet complexity requirements")

    ldap_conn = ldap_connect()
    dn = dn_for_mail(ldap_conn, mail)

    ldap_conn.modify_s(dn, [ (ldap.MOD_REPLACE, 'userPassword', str(password)) ])
    ldap_close(ldap_conn)

def create_order(email, order_lines):
    # Step 1 - Basic defense against bad data
    if (len(order_lines) > 30):
        raise CreateOrderError("Order contained more than 30 order lines.")

    if (len(order_lines) == 0):
        raise CreateOrderError("Order contained zero order lines.")

    # Step 2 - Create OrderLine records and keep an eye out for dupes
    line_records = []

    for ol in order_lines:
        # Check for invalid quantities
        if (ol['quantity'] < 1):
            raise CreateOrderError("Order has line item with quantity 0")
        if (ol['quantity'] > 20):
            raise CreateOrderError("Order has line item with quantity over 20, contact admin for large orders.")

        # Check for dupes
        dupes = 0
        for olx in order_lines:
            if olx['service_id'] == ol['service_id']:
                dupes = dupes + 1
        if dupes > 1:
            raise CreateOrderError("Order contains duplicate order lines.")

        # Find service record and create order line record
        service = models.HostingService.objects.get(id = ol['service_id'])

        # Make sure price didn't change since user added service to order
        if service.price_usd != ol['unit_price_usd']:
            raise CreateOrderError("Price of service has been changed since added to order.")

        line_rec = models.OrderLine()
        line_rec.hosting_service_id = service.id
        line_rec.quantity = ol['quantity']
        line_rec.unit_price_usd = service.price_usd

        line_records.append(line_rec)

    # Step 3 - Create the order record
    order = models.Order()
    order.account_id = account_id_for_manager(email)
    order.order_state = models.os_new
    order.submitter_email = email
    order.submit_datetime = datetime.now(tz = utc)

    # Step 4 - Calculate the fees and total
    sub_total = 0.0
    for line_rec in line_records:
        sub_total = sub_total + (
            float(line_rec.unit_price_usd) * line_rec.quantity)

    proc_fees = ((sub_total + 0.3) / 0.971) - sub_total
    proc_fees = math.floor(proc_fees * 100.0) / 100.0

    order.sub_total_usd = sub_total
    order.proc_fees_usd = proc_fees
    order.total_usd = sub_total + proc_fees

    # Step 5 - Save the order, get the order id, then save the line records
    order.save()
    for line_rec in line_records:
        line_rec.order_id = order.id
        line_rec.save()

    return order

def serialize_order(order_rec):
    # Prepare Order record
    order = {
        'id': order_rec.id,
        'account_id': order_rec.account_id,
        'order_state': order_rec.order_state,
        'submitter_email': order_rec.submitter_email,
        'submit_datetime': order_rec.submit_datetime,
        'sub_total_usd': order_rec.sub_total_usd,
        'proc_fees_usd': order_rec.proc_fees_usd,
        'total_usd': order_rec.total_usd,
        'order_lines': []
    }

    # Add OrderLines to Order record
    line_records = models.OrderLine.objects.filter(order_id = order_rec.id)
    for line_rec in line_records:
        ol = {
            'unit_price_usd': line_rec.unit_price_usd,
            'quantity': line_rec.quantity,
            'line_price_usd': float(line_rec.unit_price_usd) * line_rec.quantity
        }

        service = models.HostingService.objects.get(id = line_rec.hosting_service_id)
        ol['name'] = service.name
        ol['duration_days'] = service.duration_days
        order['order_lines'].append(ol)

    # If there is a payment associated with this Order, add that too
    payments = models.Payment.objects.filter(order_id = order_rec.id)
    if len(payments) > 0:
        szr = models.PaymentSzr(payments[0])
        order['payment'] = szr.data

    return order

def process_payment(order_id, stripe_data):
    # Step 1 - Validate that we have all the required data
    def must(field):
        if not stripe_data['card'].has_key(field) or len(stripe_data['card'][field]) == 0:
            raise PaymentDataInvalid(field)

    must('name')
    must('address_line1')
    must('address_city')
    must('address_state')
    must('address_zip')

    # Step 2 - Prepare the transaction data
    stripe.api_key = settings.STRIPE_KEY_SECRET

    order = models.Order.objects.get(id = order_id)
    card_token = stripe_data['id']
    cents = int(order.total_usd * 100)
    payment_description = "Order %i for account %s" % (order.id, order.account_id)

    # Step 3 - Charge the user's card
    charge = stripe.Charge.create(
        amount = cents,
        currency = 'usd',
        card = card_token,
        description = payment_description
    )

    # Step 4 - Write a payment record
    payment = models.Payment()
    payment.order_id = order.id
    payment.card_id = int(stripe_data['card']['last4'])
    payment.name_on_card = stripe_data['card']['name']
    payment.payment_datetime = datetime.now(tz = utc)
    payment.payment_amount = order.total_usd
    payment.charge_ref = charge.id
    payment.save()

    # Step 5 - Mark the order as paid
    order.order_state = models.os_paid
    order.save()

    # Step 6 - Invoke order fulfillment
    pid = submit_process(order.account_id, "Fulfill order %i" % order.id)
    tasks.fulfill_order.delay(pid, order.id)