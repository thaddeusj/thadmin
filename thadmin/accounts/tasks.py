import time
import re
from uuid import uuid4
from datetime import datetime
from pytz import utc
from celery import task

from thadmin.accounts import models
from thadmin.common.processes import start_process, finish_process, fail_process

@task
def test_task(pid):
    start_process(pid)
    time.sleep(30)
    finish_process(pid, "Waited 30 seconds.")

@task
def fulfill_order(pid, order_id):
    start_process(pid)

    try:
        order = models.Order.objects.get(id = order_id)
        order_lines = models.OrderLine.objects.filter(order_id = order_id)
        for ol in order_lines:
            hs = models.HostingService.objects.get(id = ol.hosting_service_id)
            codes = re.sub(r'\s*', '', hs.service_codes)
            for q in range(0, ol.quantity):
                for code in codes:
                    sc = models.ServiceCredit()
                    sc.service_credit_id = str(uuid4())
                    sc.account_id = order.account_id
                    sc.order_id = order.id
                    sc.service_code = code
                    sc.credit_state = models.scs_available
                    sc.issued_datetime = datetime.now(tz = utc)
                    sc.save()

        order.order_state = models.os_fulfilled
        order.save()
        
        finish_process(pid, "Order %i Fulfilled" % order_id)
    except Exception as e:
        fail_process(pid, "Failed to fulfill order %i: %s" % (order_id, str(e)))