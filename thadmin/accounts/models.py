from django.db import models
from rest_framework import serializers

service_codes = {
    1: 'Domain Name',
    2: 'Mailbox',
    3: 'Django Site 20/100',
    4: 'Mezzanine Site 20/100'
}

sc_domain_name = 1
sc_mailbox = 2
sc_django_site_20_100 = 3
sc_mez_site_20_100 = 4

service_credit_state = {
    0: 'Available',
    1: 'In Use',
    2: 'Consumed'
}

scs_available = 0
scs_inuse = 1
scs_consumed = 2

process_states = {
    0: 'Pending',
    1: 'Running',
    2: 'Failed',
    3: 'Complete'
}

ps_pending = 0
ps_running = 1
ps_failed = 2
ps_complete = 3

order_states = {
    0: 'New',
    1: 'Paid',
    2: 'Fulfilled',
    3: 'Cancelled'
}

os_new = 0
os_paid = 1
os_fulfilled = 2
os_cancelled = 3

class Invitation(models.Model):
    invite_code = models.CharField(max_length = 36, primary_key = True)
    issued_to_mail = models.CharField(max_length = 255)
    issued_to_name = models.CharField(max_length = 255)
    issued_datetime = models.DateTimeField()
    redeemed_datetime = models.DateTimeField(blank = True, null = True)

class InvitationSzr(serializers.ModelSerializer):
    class Meta:
        model = Invitation
        fields = ('invite_code', 'issued_to_mail',
            'issued_to_name', 'issued_datetime', 'redeemed_datetime')

class Account(models.Model):
    account_id = models.CharField(max_length = 255, primary_key = True)
    first_name = models.CharField(max_length = 123)
    last_name = models.CharField(max_length = 123)
    street_address = models.CharField(max_length = 255)
    city = models.CharField(max_length = 100)
    state = models.CharField(max_length = 25)
    zipcode = models.CharField(max_length = 11)
    phone = models.CharField(max_length = 25)
    active = models.BooleanField(default = True)

class AccountSzr(serializers.ModelSerializer):
    class Meta:
        model = Account
        fields = ('account_id', 'first_name',
            'last_name', 'street_address', 'city',
            'state', 'zipcode', 'phone')

class AccountManager(models.Model):
    email = models.CharField(max_length = 255, primary_key = True)
    account_id = models.CharField(max_length = 255, db_index = True)

class AccountManagerSzr(serializers.ModelSerializer):
    class Meta:
        model = AccountManager
        fields = ('id', 'email', 'account_id')

class HostingService(models.Model):
    name = models.CharField(max_length = 255)
    # service_codes is a space separated list of service codes to create
    # when this product is ordered. For example, the service_codes value
    # for a 2 domains and 5 mailboxes product would be "1 1 2 2 2 2 2"
    service_codes = models.CharField(max_length = 255)
    short_description = models.CharField(max_length = 255)
    duration_days = models.IntegerField()
    price_usd = models.DecimalField(max_digits = 11, decimal_places = 2)
    active = models.BooleanField(db_index = True, default = True)

class HostingServiceSzr(serializers.ModelSerializer):
    class Meta:
        model = HostingService
        fields = ('id', 'name', 'service_codes', 'short_description',
            'duration_days', 'price_usd')

class HostingServiceDescription(models.Model):
    service_id = models.IntegerField(primary_key = True)
    description = models.TextField()

class HostingServiceDescriptionSzr(serializers.ModelSerializer):
    class Meta:
        model = HostingServiceDescription
        fields = ('service_id', 'description')

class ServiceCredit(models.Model):
    service_credit_id = models.CharField(max_length = 36, primary_key = True)
    account_id = models.CharField(max_length = 255, db_index = True)
    order_id = models.IntegerField()
    service_code = models.IntegerField()
    credit_state = models.IntegerField()
    issued_datetime = models.DateTimeField()
    expires_datetime = models.DateTimeField(blank = True, null = True)
    used_for = models.CharField(max_length = 255, blank = True)

class ServiceCreditSzr(serializers.ModelSerializer):
    class Meta:
        model = ServiceCredit
        fields = ('service_credit_id', 'account_id', 'order_id',
            'service_code', 'credit_state', 'issued_datetime',
            'expires_datetime', 'used_for')

class AccountProcess(models.Model):
    account_id = models.CharField(max_length = 255, db_index = True)
    description = models.CharField(max_length = 255)
    result = models.CharField(max_length = 255, blank = True)
    process_state = models.IntegerField()
    submit_datetime = models.DateTimeField()
    end_datetime = models.DateTimeField(blank = True, null = True)

class AccountProcessSzr(serializers.ModelSerializer):
    class Meta:
        model = AccountProcess
        fields = ('id', 'account_id', 'description',
            'result', 'process_state', 'submit_datetime', 'end_datetime')

class Order(models.Model):
    account_id = models.CharField(max_length = 255, db_index = True)
    order_state = models.IntegerField()
    submitter_email = models.CharField(max_length = 255, blank = True)
    submit_datetime = models.DateTimeField(blank = True)
    sub_total_usd = models.DecimalField(max_digits = 11, decimal_places = 2, default = 0.0)
    proc_fees_usd = models.DecimalField(max_digits = 11, decimal_places = 2, default = 0.0)
    total_usd = models.DecimalField(max_digits = 11, decimal_places = 2, default = 0.0)

class OrderSzr(serializers.ModelSerializer):
    class Meta:
        model = Order
        fields = ('id', 'account_id', 'order_state',
            'submitter_email', 'submit_datetime', 'sub_total_usd',
            'proc_fees_usd', 'total_usd')

class OrderLine(models.Model):
    order_id = models.IntegerField(db_index = True)
    hosting_service_id = models.IntegerField()
    quantity = models.IntegerField()
    unit_price_usd = models.DecimalField(max_digits = 11, decimal_places = 2)

class OrderLineSzr(serializers.ModelSerializer):
    class Meta:
        model = OrderLine
        fields = ('id', 'order_id', 'hosting_service_id',
            'quantity', 'unit_price_usd')

class Payment(models.Model):
    order_id = models.IntegerField(db_index = True)
    card_id = models.IntegerField()
    name_on_card = models.CharField(max_length = 255)
    payment_datetime = models.DateTimeField()
    payment_amount = models.DecimalField(max_digits = 11, decimal_places = 2)
    charge_ref = models.CharField(max_length = 64)

class PaymentSzr(serializers.ModelSerializer):
        class Meta:
            model = Payment
            fields = ('id', 'order_id', 'card_id', 'name_on_card',
                'payment_datetime', 'payment_amount', 'charge_ref')