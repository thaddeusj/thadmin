/*******************************************************************************
 * ACCOUNT SUMMARY
 ******************************************************************************/
function AccountSummaryController($scope, $rootScope, $http, $routeParams) {
    $scope.load_summary = function(account_id) {
        set_loading($scope, true);

        $http.get('/accounts/accounts/' + account_id + '/summary')
            .success(function(data, status, headers, config) {
                $scope.summary = data;
                set_loading($scope, false);
            });
    };

    $scope.concise_uuid = function(uuid) {
        return uuid.substring(30, 36);
    };

    $scope.load_summary($routeParams.account_id);
}

/*******************************************************************************
 * SERVICE LIST
 ******************************************************************************/
function ServiceListController($scope, $http) {
    $scope.services = [];

    $scope.load_service_list = function() {
        set_loading($scope, true);

        $http.get('/accounts/services')
            .success(function(data, status, headers, config) {
                set_loading($scope, false);
                $scope.services = data;
            });
    };

    $scope.load_service_list();
}

/*******************************************************************************
 * SERVICE EDIT
 ******************************************************************************/
function ServiceEditController($scope, $http, $location, $routeParams) {
    $scope.load_record = function(service_id) {
        set_loading($scope, true);

        $http.get('/accounts/services/' + service_id)
            .success(function(data, status, headers, config) {
                set_loading($scope, false);
                $scope.record = data;
            });
    }

    $scope.save_record = function() {
        set_saving($scope, true);

        if ($scope.record['id'] == 0) {
            delete $scope.record['id'];

            $http.post('/accounts/services/', $scope.record)
                .success(function(data, status, headers, config) {
                    set_saving($scope, false);
                    $location.path('/accounts/services/' + data);
                })
                .error(function(data, status, headers, config) {
                    set_saving($scope, false);
                    $scope.errors = data;
                });
        } else {
            $http.put('/accounts/services/' + $scope.record['id'], $scope.record)
                .success(function(data, status, headers, config) {
                    set_saving($scope, false);
                })
                .error(function(data, status, headers, config) {
                    set_saving($scope, false);
                    $scope.errors = data;
                });
        }
    }

    $scope.show_confirm_delete = function() {
        $scope.confirm_delete = true;
    }

    $scope.delete_record = function() {
        set_saving($scope, true);
        $http['delete']('/accounts/services/' + $scope.record['id'])
            .success(function(data, status, headers, config) {
                set_saving($scope, false);
                $location.path('/accounts/services');
            });
    }

    if ($routeParams.service_id > 0) {
        $scope.load_record($routeParams.service_id);
    } else {
        $scope.record = { 'id': 0 }
        $scope.loading = false;
    }
}

/*******************************************************************************
 * ACCOUNTS LIST
 ******************************************************************************/
function AccountsListController($scope, $http, $location) {
    $scope.load_account_list = function() {
        set_loading($scope, true);
        $http.get('/accounts/accounts')
            .success(function(data, status, headers, config) {
                $scope.accounts = data;
                set_loading($scope, false);
            });
    };

    $scope.load_account_list();
}

/*******************************************************************************
 * INVITATION LIST
 ******************************************************************************/
function InvitationListController($scope, $http, $location) {
    $scope.shown_delete_confirm = {};
    $scope.shown_create_form = false;
    $scope.new_invite = {};

    $scope.load_invites = function() {
        set_loading($scope, true);
        $http.get('/accounts/invitations/')
            .success(function(data, status, headers, config) {
                $scope.invitations = data;
                set_loading($scope, false);
            })
    };

    $scope.delete_invite = function(invite_code, really, $event) {
        $event.stopPropagation();

        if (! really) {
            $scope.shown_delete_confirm[invite_code] = true;
            var td_id = 'edit_' + invite_code;
            $('#' + td_id).addClass('red_bg');
        } else {
            if ($scope.shown_delete_confirm[invite_code] != true) { return; }
            $http['delete']('/accounts/invitations/' + invite_code)
                .success(function(data, status, headers, config) {
                    $scope.load_invites();
                });
        }
    };

    $scope.show_create_form = function() { $scope.shown_create_form = true; };

    $scope.create_invite = function() {
        set_saving($scope, true);
        $http.post('/accounts/invitations/', $scope.new_invite)
            .success(function(data, status, headers, config) {
                set_saving($scope, false);
                $scope.new_invite = {};
                $scope.shown_create_form = false;
                $scope.load_invites();
            });
    };

    $scope.load_invites();
}

/*******************************************************************************
 * REDEEM INVITATION
 ******************************************************************************/
function RedeemInviteController($scope, $http, $location) {
    $scope.redeem_invite = function() {
        set_saving($scope, true);
        $http.post('/accounts/invitations/' + $scope.invite_code + '/redeem')
            .success(function(data, status, headers, config) {
                set_saving($scope, false);
                $location.path('/accounts/create_account');
            })
            .error(function(data, status, headers, config) {
                set_saving($scope, false);
                $scope.invite_invalid = true;
            });
    };
}

/*******************************************************************************
 * CREATE ACCOUNT
 ******************************************************************************/
function CreateAccountController($scope, $rootScope, $http, $location) {
    $scope.errors = {}
    $scope.account = {}

    $scope.create_account = function() {
        set_saving($scope, true);

        $http.post('/accounts/accounts/create', $scope.account)
            .success(function(data, status, headers, config) {
                $rootScope.new_account_id = JSON.parse(data);
                set_saving($scope, false);
                $location.path('/accounts/account_created');
            })
            .error(function(data, status, headers, config) {
                set_saving($scope, false);
                $scope.errors = data;
            });
    };
}

/*******************************************************************************
 * POST CREATE ACCOUNT
 ******************************************************************************/
function PostCreateAccountController($scope, $rootScope, $http, $location) {
    $scope.account_id = $rootScope.new_account_id;
    $scope.account_mail = $scope.account_id + '@thogan.com';
}

/*******************************************************************************
 * SET PASSWORD
 ******************************************************************************/
function SetPasswordController($scope, $http, $location, $timeout) {
    $scope.password = '';
    $scope.confirm_password = '';
    $scope.pass_match = true;
    $scope.validate_timeout = null;

    $scope.not_valid = true;
    $scope.rules = {
        'rules': {
            'min_len': 'fail',
            'repeat': 'pass',
            'longpass': 'na',
            '4letters': 'fail',
            '1number': 'fail',
            '1symbol': 'fail'
        },
        'valid': false
    };

    $scope.password_changed = function() {
        if ($scope.validate_timeout) {
            $timeout.cancel($scope.validate_timeout);
        }

        $scope.confirm_changed();
        $scope.validate_timeout = $timeout(function() {
            $scope.validate_timeout = null;
            $http.post('/accounts/password/validate', JSON.stringify($scope.password))
                .success(function(data, status, headers, config) {
                    $scope.rules = data;
                });
        }, 500, true);

    };

    $scope.confirm_changed = function() {
        $scope.pass_match = $scope.password == $scope.confirm_password;
    };

    $scope.set_password = function() {
        set_saving($scope, true);

        $http.post('/accounts/password/set', JSON.stringify($scope.password))
            .success(function(data, status, headers, config) {
                set_saving($scope, false);
                $location.path('/accounts/welcome');
            });
    };
}