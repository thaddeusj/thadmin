/*******************************************************************************
 * CURRENT ORDER CONTROLLER
 ******************************************************************************/
function CurrentOrderController($scope, $rootScope, $http, $location) {
    set_loading($scope, true);
    $scope.order_subtotal = 0;
    $scope.order_fees = 0;
    $scope.order_total = 0;

    $scope.load_service_list = function() {
        $http.get('/accounts/services/')
            .success(function(data, status, headers, config) {
               $scope.services = data;
                set_loading($scope, false);
            });
    };

    $scope.calc_totals = function() {
        $scope.order_subtotal = 0;
        for (idx in $rootScope.current_order) {
            $scope.order_subtotal = $scope.order_subtotal +
                $rootScope.current_order[idx].line_price_usd;
        }

        $scope.order_fees = (($scope.order_subtotal + 0.3) / 0.971) -
            $scope.order_subtotal;
        // Avoid funny rounding differences with the backend
        $scope.order_fees = Math.floor($scope.order_fees * 100.0) / 100.0;

        $scope.order_total = $scope.order_subtotal + $scope.order_fees;
    };

    $scope.add_to_order = function(idx) {
        var svc = $scope.services[idx];

        var line_item = {
            service_id: svc.id,
            selected: false,
            name: svc.name,
            duration_days: svc.duration_days,
            unit_price_usd: parseFloat(svc.price_usd),
            quantity: 1,
            line_price_usd: parseFloat(svc.price_usd)
        };

        $rootScope.current_order.push(line_item);
        localStorage.current_order = JSON.stringify($rootScope.current_order);

        $scope.calc_totals();

        scroll_to('order_top', 500);
    };

    $scope.quantity_changed = function(idx) {
        var line_item = $rootScope.current_order[idx];
        if (line_item.quantity > 20) { line_item.quantity = 20; }
        if (line_item.quantity < 0) { line_item.quantity = 0; }

        line_item.line_price_usd =
            line_item.unit_price_usd * line_item.quantity;
        localStorage.current_order = JSON.stringify($rootScope.current_order);

        $scope.calc_totals();
    };

    $scope.remove_order_lines = function() {
        var new_order = [];
        
        for (idx in $rootScope.current_order) {
            if ($rootScope.current_order[idx].selected == false) {
                new_order.push($rootScope.current_order[idx]);
            }
        }

        $rootScope.current_order = new_order;
        localStorage.current_order = JSON.stringify($rootScope.current_order);

        $scope.calc_totals();
    };

    $scope.toggle_all = function() {
        for (idx in $rootScope.current_order) {
            $rootScope.current_order[idx].selected =
                ! $rootScope.current_order[idx].selected;
        }
    };

    $scope.order_contains = function(id) {
        var found = false;
        for (idx in $rootScope.current_order) {
            if ($rootScope.current_order[idx].service_id == id) {
                found = true;
            }
        }
        return found;
    };

    $scope.check_out = function() {
        set_saving($scope, true);
        $http.post('/accounts/orders/', $rootScope.current_order)
            .success(function(data, status, headers, config) {
                $rootScope.order_cache = data;
                $rootScope.current_order = [];
                localStorage.current_order = JSON.stringify([]);
                set_saving($scope, false);
                $location.path('/accounts/orders/' + data.id);
            })
            .error(function(data, status, headers, config) {
                $scope.order_error = JSON.parse(data);
                set_saving($scope, false);
            });
    };

    if ($rootScope.current_order.length > 0) {
        $scope.calc_totals();
    }

    $scope.load_service_list();
}

/*******************************************************************************
 * ORDER DETAIL CONTROLLER
 ******************************************************************************/
function OrderDetailController($scope, $rootScope, $http, $location, $routeParams) {
    $scope.card_data = {};
    $scope.cc_errors = {};

    $scope.load_address = function(account_id) {
        set_loading($scope, true, 'address_loading', 'address_loading');

        $http.get('/accounts/accounts/' + account_id)
            .success(function(data, status, headers, config) {
                $scope.card_data.address = data.street_address;
                $scope.card_data.city = data.city;
                $scope.card_data.state = data.state;
                $scope.card_data.zipcode = data.zipcode;
                set_loading($scope, false, 'address_loading', 'address_loading');
            })
            .error(function(data, status, headers, config) {
                set_loading($scope, false, 'address_loading', 'address_loading');
            });
    };

    $scope.load_order = function() {
        set_loading($scope, true);
        var order_id = $routeParams.order_id;

        if ($rootScope.order_cache && $rootScope.order_cache.id == order_id) {
            $scope.order = $rootScope.order_cache;
            delete $rootScope.order_cache;

            if ($scope.order.order_state == $rootScope.os_new) {
                $scope.load_address($scope.order.account_id);
            }

            set_loading($scope, false);
        } else {
            $http.get('/accounts/orders/' + order_id)
                .success(function(data, status, headers, config) {
                    $scope.order = data;

                    if ($scope.order.order_state == $rootScope.os_new) {
                        $scope.load_address($scope.order.account_id);
                    }

                    set_loading($scope, false);
                });
        }
    };

    $scope.stripe_error = function(error) {
        if (error.code) {
            if (error.code == 'incorrect_number' ||
                error.code == 'invalid_number') {
                $scope.cc_errors.error_text = "Invalid Credit Card Number";
                $scope.cc_errors.card_num = true;
            } else if (error.code == 'invalid_expiry_month' ||
                       error.code == 'invalid_expiry_year') {
                $scope.cc_errors.error_text = "Invalid Expiration Date";
                $scope.cc_errors.expiration = true;
            } else if (error.code == 'invalid_cvc' ||
                       error.code == 'incorrect_cvc') {
                $scope.cc_errors.error_text = "Invalid Card Verification Code";
                $scope.cc_errors.cvc = true;
            } else if (error.code == 'expired_card') {
                $scope.cc_errors.error_text = "Provided credit card has expired.";
            } else if (error.code == 'incorrect_zip') {
                $scope.cc_errors.error_text = "ZIP Code does not match billing address for provided credit card.";
            } else if (error.code == 'card_declined') {
                $scope.cc_errors.error_text = "Card declined.";
            } else if (error.code == 'processing_error') {
                $scope.cc_errors.error_text = "Error with payment processor, please try again shortly.";
            } else if (error.code == 'missing_field') {
                $scope.cc_errors.error_text = "Please complete the form below including Name on Card and a full Billing Address.";
                $scope.cc_errors[error.field] = true;
            }
        } else {
            $scope.cc_errors.error_text = "Credit card rejected by payment processor, please validate your card details.";
        }
    }

    $scope.stripe_response = function(status, response) {
        // Check for error
        if (response.error) {
            $scope.stripe_error(response.error);
            set_saving($scope, false);
            $scope.$apply();
            return;
        }

        // Sumbit payment to backend
        $http.post('/accounts/orders/' + $scope.order.id + '/pay', response)
            .success(function(data, status, headers, config) {
                set_saving($scope, false);
                $scope.load_order();
            })
            .error(function(data, status, headers, config) {
                $scope.stripe_error(data);
                set_saving($scope, false);
            });
    };

    $scope.place_order = function() {
        $scope.cc_errors = {};

        // Validate expiration date format
        if ($scope.card_data.expiration.length != 5) {
            $scope.cc_errors.error_text = "Please enter the expiration date in MM/YY format.";
            $scope.cc_errors.expiration = true;
            return;
        }

        set_saving($scope, true);
        
        var exp_month = $scope.card_data.expiration.substring(0, 2);
        var exp_year = $scope.card_data.expiration.substring(3, 5);

        Stripe.setPublishableKey(STRIPE_KEY_PUBLIC);

        Stripe.card.createToken({
            number: $scope.card_data.card_num,
            cvc: $scope.card_data.cvc,
            exp_month: exp_month,
            exp_year: exp_year,
            name: $scope.card_data.name,
            address_line1: $scope.card_data.address,
            address_city: $scope.card_data.city,
            address_state: $scope.card_data.state,
            address_zip: $scope.card_data.zipcode
        }, $scope.stripe_response);
    };
    
    $scope.load_order();
}