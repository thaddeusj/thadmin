from datetime import datetime
from pytz import utc

from thadmin.accounts import models

def submit_process(account_id, description):
    proc = models.AccountProcess()
    proc.account_id = account_id
    proc.description = description
    proc.process_state = models.ps_pending
    proc.submit_datetime = datetime.now(tz = utc)
    proc.save()

    return proc.id

def start_process(process_id):
    proc = models.AccountProcess.objects.get(id = process_id)
    proc.process_state = models.ps_running
    proc.save()

def finish_process(process_id, result, success = True):
    proc = models.AccountProcess.objects.get(id = process_id)
    if success:
        proc.process_state = models.ps_complete
    else:
        proc.process_state = models.ps_failed
    proc.result = result
    proc.end_datetime = datetime.now(tz = utc)
    proc.save()

def fail_process(process_id, result):
    finish_process(process_id, result, False)