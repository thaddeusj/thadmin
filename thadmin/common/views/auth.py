from rest_framework.views import APIView
from rest_framework import status
from rest_framework.response import Response
from rest_framework import permissions

from django.contrib.auth import authenticate, login, logout

from thadmin import no_cache_headers
from thadmin.common.serializers.auth import MailAndPass
from thadmin.accounts.services import account_id_for_manager

class Login(APIView):
    permission_classes = (permissions.AllowAny,)

    def post(self, request, format = None):
        szr = MailAndPass(data = request.DATA)
        if szr.is_valid():
            user = authenticate(username = szr.object['email'], password = szr.object['password'])
            if user is not None and user.is_active:
                login(request, user)

                if request.user.is_superuser:
                    account_id = 'Admin'
                else:
                    account_id = account_id_for_manager(request.user.username)
                    
                userinfo = {
                    'email': request.user.username,
                    'is_superuser': request.user.is_superuser,
                    'account_id': account_id,
                }
                return Response(userinfo, status = status.HTTP_200_OK)

        return Response('', status = status.HTTP_400_BAD_REQUEST)

class Logout(APIView):
    permission_classes = (permissions.AllowAny,)

    def post(self, request, format = None):
        logout(request)
        return Response('', status = status.HTTP_200_OK)

class UserInfo(APIView):
    def get(self, request, format = None):
        if request.user.is_superuser:
            account_id = 'Admin'
        else:
            account_id = account_id_for_manager(request.user.username)

        userinfo = {
            'email': request.user.username,
            'is_superuser': request.user.is_superuser,
            'account_id': account_id,
        }
        return Response(userinfo, headers = no_cache_headers);