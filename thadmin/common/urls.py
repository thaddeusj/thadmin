from django.conf.urls import patterns, include, url
from thadmin.common.views import auth

urlpatterns = patterns('',
    url(r'^auth/login$', auth.Login.as_view()),
    url(r'^auth/logout$', auth.Logout.as_view()),
    url(r'^auth/userinfo$', auth.UserInfo.as_view()),
)