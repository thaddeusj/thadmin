from django.conf.urls import patterns, include, url
from thadmin.zoneman.views import zones

urlpatterns = patterns('',
    url(r'^$', zones.ZoneList.as_view()),
    url(r'^(?P<zone_name>[^/]+)/records$', zones.ZoneRecordList.as_view()),
)