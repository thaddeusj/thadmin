import ldap
from django_auth_ldap.config import LDAPSearch, GroupOfUniqueNamesType

DEBUG = True
TEMPLATE_DEBUG = DEBUG

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': '/home/thogan/thadmin/dev.db',
        'USER': '',
        'PASSWORD': '',
        'HOST': '',
        'PORT': '',
    }
}

AUTH_LDAP_SERVER_URI = 'ldap://localhost:1389'
AUTH_LDAP_BIND_DN = ''
AUTH_LDAP_BIND_PASSWORD = ''
AUTH_LDAP_USER_SEARCH = LDAPSearch('ou=Users,dc=thogan,dc=lan', ldap.SCOPE_SUBTREE, '(mail=%(user)s)')

AUTH_LDAP_GROUP_SEARCH = LDAPSearch('ou=Groups,dc=thogan,dc=lan', ldap.SCOPE_SUBTREE,
    '(objectClass=groupOfUniqueNames)')
AUTH_LDAP_GROUP_TYPE = GroupOfUniqueNamesType()

AUTH_LDAP_USER_FLAGS_BY_GROUP = {
    'is_active': 'cn=hostedservices,ou=Groups,dc=thogan,dc=lan',
    'is_staff': 'cn=unixadmin,ou=Groups,dc=thogan,dc=lan',
    'is_superuser': 'cn=unixadmin,ou=Groups,dc=thogan,dc=lan'
}

EMAIL_HOST = 'mailext2.goliath.thogan.lan'
EMAIL_PORT = 25

BROKER_URL = 'amqp://thadmin:thadmin@localhost:5672/thadmin'

ZM_NS = '127.0.0.1'
ZM_KEY_ALGO = 'HMAC_SHA512'
ZM_KEY_NAME = 'key_zoneman'
ZM_KEY = '' # TSIG KEY HERE

LDAP_MGR_URI = 'ldap://localhost:1389'
LDAP_MGR_BIND_DN = 'cn=root'
LDAP_MGR_BIND_PASS = '' # LDAP MANAGER PASSWORD

LDAP_OU_USERS = 'ou=Users,dc=thogan,dc=lan'
LDAP_OU_MAILGROUPS = 'ou=MailGroups,dc=thogan,dc=lan'
LDAP_HS_GROUP = 'cn=hostedservices,ou=Groups,dc=thogan,dc=lan'


STRIPE_KEY_SECRET = '' # STRIPE SECRET KEY
STRIPE_KEY_PUBLIC = '' # STRIPE PUBLIC KEY